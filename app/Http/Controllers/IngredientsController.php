<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;

class IngredientsController extends Controller
{
    /* Displays list of ingredients in db */
    public function display()
    {
        $ingredients = Ingredient::all();
        return view('list', compact('ingredients'));
    }

}

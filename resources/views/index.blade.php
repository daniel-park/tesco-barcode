<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tesco</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

        <!-- Styles -->
        <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
        <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <!--    <script type="text/javascript" src="live_w_locator.js"></script>-->


        <style>

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }







            body {
                background-color: #FFF;
                margin: 0px;
                font-family: Ubuntu, sans-serif;
                color: #1e1e1e;
                font-weight: normal;
                padding-top: 0;
            }

            h1, h2, h3, h4 {

                /*font-family: "Cabin Condensed", sans-serif;*/
            }

            header {
                /*background: #FFC600;*/
                /*padding: 1em;*/
            }

            header .headline {
                max-width: 640px;
                padding-left: 1em;
                padding-right: 1em;
                /* margin-top: 0; */
                position: absolute;
                left: 0;
                right: 0;
                margin-left: auto;
                margin-right: auto;

            }

            header .headline h1 {
                /*color: #FFDD69;*/
                font-size: 2.5em;
                margin-bottom: 0;
                margin-top: 0.1em;
                font-family: 'Pacifico', cursive;
                /*color: #59AD5B;*/
                color: #FFFFFF;
                text-align: center;

            }

            header .headline h2 {
                margin-top: 0.2em;
            }

            svg {
                /*position: absolute;*/
                bottom: 0;
                width: 100%;
                height: 100px;
            }
            footer {
                background: #0A4DB7;
                color: #6C9CE8;
                padding: 1em 2em 2em;
            }

            #container {
                width: 640px;
                margin: 20px auto;
                padding: 10px;
            }

            #interactive.viewport {
                width: 640px;
                height: 480px;
            }


            #interactive.viewport canvas, video {
                float: left;
                width: 640px;
                height: 480px;
            }

            #interactive.viewport canvas.drawingBuffer, video.drawingBuffer {
                margin-left: -640px;
            }

            .controls fieldset {
                border: none;
                margin: 0;
                padding: 0;
            }

            .controls .input-group {
                float: left;
            }

            .controls .input-group input, .controls .input-group button {
                display: block;
            }

            .controls .reader-config-group {
                float: right;
            }

            .controls .reader-config-group label {
                display: block;
            }

            .controls .reader-config-group label span {
                width: 9rem;
                display: inline-block;
                text-align: right;
            }

            .controls:after {
                content: '';
                display: block;
                clear: both;
            }


            #result_strip {
                margin: 10px 0;
                border-top: 1px solid #EEE;
                border-bottom: 1px solid #EEE;
                padding: 10px 0;
            }

            #result_strip > ul {
                padding: 0;
                margin: 0;
                list-style-type: none;
                width: auto;
                overflow-x: auto;
                overflow-y: hidden;
                white-space: nowrap;
            }

            #result_strip > ul > li {
                display: inline-block;
                vertical-align: middle;
                width: 160px;
            }

            #result_strip > ul > li .thumbnail {
                padding: 5px;
                margin: 4px;
                border: 1px dashed #CCC;
            }

            #result_strip > ul > li .thumbnail img {
                max-width: 140px;
            }

            #result_strip > ul > li .thumbnail .caption {
                white-space: normal;
            }

            #result_strip > ul > li .thumbnail .caption h4 {
                text-align: center;
                word-wrap: break-word;
                height: 40px;
                margin: 0px;
            }

            #result_strip > ul:after {
                content: "";
                display: table;
                clear: both;
            }


            .scanner-overlay {
                display: none;
                width: 640px;
                height: 510px;
                position: absolute;
                padding: 20px;
                top: 50%;
                margin-top: -275px;
                left: 50%;
                margin-left: -340px;
                background-color: #FFF;
                -moz-box-shadow: #333333 0px 4px 10px;
                -webkit-box-shadow: #333333 0px 4px 10px;
                box-shadow: #333333 0px 4px 10px;
            }

            .scanner-overlay > .header {
                position: relative;
                margin-bottom: 14px;
            }

            .scanner-overlay > .header h4, .scanner-overlay > .header .close {
                line-height: 16px;
            }

            .scanner-overlay > .header h4 {
                margin: 0px;
                padding: 0px;
            }

            .scanner-overlay > .header .close {
                position: absolute;
                right: 0px;
                top: 0px;
                height: 16px;
                width: 16px;
                text-align: center;
                font-weight: bold;
                font-size: 14px;
                cursor: pointer;
            }


            i.icon-24-scan {
                width: 24px;
                height: 24px;
                background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QzFFMjMzNTBFNjcwMTFFMkIzMERGOUMzMzEzM0E1QUMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QzFFMjMzNTFFNjcwMTFFMkIzMERGOUMzMzEzM0E1QUMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDMUUyMzM0RUU2NzAxMUUyQjMwREY5QzMzMTMzQTVBQyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDMUUyMzM0RkU2NzAxMUUyQjMwREY5QzMzMTMzQTVBQyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtQr90wAAAUuSURBVHjanFVLbFRVGP7ua97T9DGPthbamAYYBNSMVbBpjCliWWGIEBMWsnDJxkh8RDeEDW5MDGticMmGBWnSlRSCwgLFNkqmmrRIqzjTznTazkxn5s7c6/efzm0G0Jhwkj/nP+d/nv91tIWFBTQaDQWapkGW67p4ltUub5qmAi0UCqF/a/U2m81tpmddotwwDGSz2dzi4uKSaOucnJycGhsbe1XXdQiIIcdxEAgEtgXq9brySHCht79UXi/8QheawN27d385fPjwuEl6XyKR6LdtW7t06RLK5TKOHj2K/fv3Q87Dw8OYn5/HiRMnMDs7i5mZGQwODiqlPp8PuVwO6XRaOXb16lXl1OnTp5FMJvtosF8M+MWLarWqGJaWlpBKpRRcu3YN4+PjmJ6exsTEhDJw5coVjI6OKgPhcBiZTAbxeBx+vx+XL19Gd3c3Tp48Ka9zqDYgBlTQxYNgMIhIJKLCILkQb+TZsgvdsiyFi+feWRR7oRNZyanQtvW2V4DEUUBiK2eJpeDirSyhCe7F2QPh8fiEp72i9PbsC5G52DbiKZA771yr1dTuGfJ4PQNPFoAyQNR1aNEmsS5eyB3PgjeooMZd2AWvNmzYci/Gea7TeFOcI93jV/K67noGmi4vdRI9gPSDeMLSdKUBZZczlWm1rTtHjLZ24d+WER2tc8N1m+Y+ID74wx0zGYvhg9UNrJdtHJyZRdQfwPsrq9g99xsGlgsYmr6BNzO/IVwsYfjBQ6XYz6JI/72MV366B5/lw0elOkJWGUM3bmKtWjXSLuLaBWhnPnnp0FfoiFi4+TMfVAb2poBkDLjO845uYLEAjL4ALGWBP5YAOsP4AJYBFDaB1HOSVWD2PuV95H2RdV93Lv74/cf6p6Zxq/h6OofeOPJBC39JtONdwOAAViOs4p4OFGTf0Uc8iiyrr9YdQrUnDLsngrVOC0jQib44HlF2RafRZBz1Qy+vfhgK3NJZBlrm+LEm9qWwzFgLU7Ozg0JxZP06jQSRpQ7EerAWDSt6PuhHPmChEAog56fCLvJT5hHTm3OZkz3DyLx7XNWTGEA1GkV14gjWgwbW0ESVjYRwCOuai03L5E7OUBAV4kXSS4auoGIaKOma4m8EA5R1sMEGLh95C+XuLph0WJWpxepYYLtfT0RRgY1KgNODY6BoaChRuEhDCIZQYseuki5KN6hcQHiq7OZNv4/Zq2O6P4Lfkwn46vZjjaYZrIpvWbpzjLErrc4xUGE4avRedpYJalRcIl5hQius/SrPm9xrNOQYJhao6BvNUeWqtY8KaWuNjHOFAr7mM9f4NA4UbKysoUJ8PV9UzVOx6wxDDWUOxnK1pmCD07fOMAvtIsM3l89Dl3HRGhVma9AZMqjOnz2LQqWCxs6dqr3T7x1DTzKJaG8SekcHhg4cgI/56uKdlKnBV/WndqN3YAB/7tyBd3oT6GBIOzs7kc/nDfFdDFT5bS73cp06dQoaPa/Rw/rtO/resTHxxE2m9rCrbSR27UJCcMf1BpiA5rAAGgdfc868fUR1sMwj0cm9Iu9IctweisViB3hhKTHDcHc5jv/LspbyaZrR1OD82/fIlOkuB9LnEWRmDX2TsddUPg3D5gvuc0je0rZaD5EW6G3yjS+A3eeBEWq3XW/Abw1HhUspXADufQb86oW7tZytkYCN//3hHwBvDALPi8EnSOYK8DAOfCc2h4aGcO7cuafkzampqf9UripH12/DtOZbx8ciVGzYy5OO40o25ascGRl5Ssc/AgwAjW3JwqIUjSYAAAAASUVORK5CYII=");
                display: inline-block;
                background-repeat: no-repeat;
                line-height: 24px;
                margin-top: 1px;
                vertical-align: text-top;
            }

            @media (max-width: 603px) {

                #container {
                    width: 300px;
                    margin: 10px auto;
                    -moz-box-shadow: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                }

                #container form.voucher-form input.voucher-code {
                    width: 180px;
                }
            }
            @media (max-width: 603px) {

                .reader-config-group {
                    width: 100%;
                }

                .reader-config-group label > span {
                    width: 50%;
                }

                .reader-config-group label > select, .reader-config-group label > input {
                    max-width: calc(50% - 2px);
                }

                #interactive.viewport {
                    width: 300px;
                    height: 300px;
                    overflow: hidden;
                }


                #interactive.viewport canvas, video {
                    margin-top: -50px;
                    width: 300px;
                    height: 400px;
                }

                #interactive.viewport canvas.drawingBuffer, video.drawingBuffer {
                    margin-left: -300px;
                }


                #result_strip {
                    margin-top: 5px;
                    padding-top: 5px;
                }

                #result_strip ul.thumbnails > li {
                    width: 150px;
                }

                #result_strip ul.thumbnails > li .thumbnail .imgWrapper {
                    width: 130px;
                    height: 130px;
                    overflow: hidden;
                }

                #result_strip ul.thumbnails > li .thumbnail .imgWrapper img {
                    margin-top: -25px;
                    width: 130px;
                    height: 180px;
                }
            }
            @media (max-width: 603px) {

                .overlay.scanner {
                    width: 640px;
                    height: 510px;
                    padding: 20px;
                    margin-top: -275px;
                    margin-left: -340px;
                    background-color: #FFF;
                    -moz-box-shadow: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                }

                .overlay.scanner > .header {
                    margin-bottom: 14px;
                }

                .overlay.scanner > .header h4, .overlay.scanner > .header .close {
                    line-height: 16px;
                }

                .overlay.scanner > .header .close {
                    height: 16px;
                    width: 16px;
                }
            }



        </style>

    </head>
    <body>








    <header>
        <div class="headline">
            <h1>Food Checker</h1>
            <!--            <h2>Find suitability of your food</h2>-->
        </div>

        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 1081.2 389.8" style="enable-background:new 0 0 1081.2 389.8;" preserveAspectRatio="none">
            <style type="text/css">
                .st0{fill:#454741;}
            </style>
            <polygon id="XMLID_4_" class="st0" points="1081.2,0 0,0 0,303.2 0.6,303.2 0.6,389.8 11.9,389.8 11.9,303.2 23.3,303.2 23.3,389.8
	34.7,389.8 34.7,303.2 68.8,303.2 68.8,364.9 80.2,364.9 80.2,303.2 91.5,303.2 91.5,364.9 102.9,364.9 114.3,364.9 114.3,303.2
	125.6,303.2 125.6,364.9 137,364.9 137,303.2 159.7,303.2 159.7,364.9 171.1,364.9 182.5,364.9 193.8,364.9 193.8,303.2
	205.2,303.2 205.2,364.9 216.6,364.9 227.9,364.9 227.9,303.2 250.7,303.2 250.7,364.9 262.1,364.9 273.4,364.9 273.4,303.2
	296.2,303.2 296.2,364.9 307.5,364.9 307.5,303.2 330.3,303.2 330.3,364.9 341.6,364.9 353,364.9 353,303.2 364.4,303.2
	364.4,364.9 375.7,364.9 387.1,364.9 398.5,364.9 409.8,364.9 409.8,303.2 421.2,303.2 421.2,364.9 432.6,364.9 432.6,303.2
	455.3,303.2 455.3,364.9 466.7,364.9 478.1,364.9 489.4,364.9 489.4,303.2 500.8,303.2 500.8,364.9 512.2,364.9 512.2,303.2
	523.5,303.2 523.5,389.8 534.9,389.8 534.9,303.2 546.3,303.2 546.3,389.8 557.6,389.8 557.6,303.2 569,303.2 569,364.9
	580.4,364.9 591.7,364.9 591.7,303.2 614.5,303.2 614.5,364.9 625.8,364.9 637.2,364.9 637.2,303.2 648.6,303.2 648.6,364.9
	659.9,364.9 671.3,364.9 671.3,303.2 682.7,303.2 682.7,364.9 694.1,364.9 705.4,364.9 705.4,303.2 728.2,303.2 728.2,364.9
	739.5,364.9 739.5,303.2 785,303.2 785,364.9 796.4,364.9 796.4,303.2 807.7,303.2 807.7,364.9 819.1,364.9 819.1,303.2
	830.5,303.2 830.5,364.9 841.8,364.9 853.2,364.9 864.6,364.9 864.6,303.2 887.3,303.2 887.3,364.9 898.7,364.9 898.7,303.2
	921.4,303.2 921.4,364.9 932.8,364.9 944.2,364.9 955.5,364.9 955.5,303.2 966.9,303.2 966.9,364.9 978.3,364.9 978.3,303.2
	1012.4,303.2 1012.4,364.9 1023.7,364.9 1023.7,303.2 1046.5,303.2 1046.5,389.8 1057.8,389.8 1057.8,303.2 1069.2,303.2
	1069.2,389.8 1080.6,389.8 1080.6,303.2 1081.2,303.2 "/>
        </svg>



    </header>
    <section id="container" class="container">

<h3>Search result... </h3>

<!--        <div class="flex-center position-ref full-height">-->
<!--            @if (Route::has('login'))-->
<!--                <div class="top-right links">-->
<!--                    @auth-->
<!--                        <a href="{{ url('/home') }}">Home</a>-->
<!--                    @else-->
<!--                        <a href="{{ route('login') }}">Login</a>-->
<!--                        <a href="{{ route('register') }}">Register</a>-->
<!--                    @endauth-->
<!--                </div>-->
<!--            @endif-->
<!---->
<!--            <div class="content">-->
<!--                <div class="title m-b-md">-->
<!--                    Tesco-->
<!--                </div>-->
<!---->
<!--                <div class="links">-->
<!--                    <a href="https://laravel.com/docs">Documentation</a>-->
<!--                    <a href="https://laracasts.com">Laracasts</a>-->
<!--                    <a href="https://laravel-news.com">News</a>-->
<!--                    <a href="https://forge.laravel.com">Forge</a>-->
<!--                    <a href="https://github.com/laravel/laravel">GitHub</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->




<!--WORKING-->
<!--       <div id="demo">-->
<!--           <h2 id="description"></h2>-->
<!--           <p id="ingredients"></p>-->
<!--        </div>-->
<!---->






<!--<script>-->
<!--    var data = $.get("list.blade.php", { name:barcode });-->
<!--    console.log (data);-->
<!--</script>-->


<!---->
<?php
if(isset($_GET['id'])){
    $product_id = $_GET['id'];
//    echo $product_id;
}

require_once 'HTTP/Request2.php';
$request = new Http_Request2('https://dev.tescolabs.com/product/');
$request->setConfig(array(
        'proxy_host' => 'wwwproxy.hud.ac.uk',
        'proxy_port' => 3128

));
$url = $request->getUrl();
$headers = array(
    // Request headers
    'Ocp-Apim-Subscription-Key' => env('TESCO'),
);

$request->setHeader($headers);

$parameters = array(

//        Request parameters
//        cod
//        'gtin' => '05052004435789',
        'gtin' => $product_id,
//        Pepperoni Pizza
//        "gtin"=> '05054402006097',
//        Spicy Pepperoni Slices which are made with Pork
//        'tpnc' => '266510588',
//        Tesco Finest 10 British Pork Cocktail Sausages In Streaky Bacon 210G. Does not show ingredient array but data is on webpage
//        "gtin"=> '05053947841835',

//    'tpnb' => '{string}',
//    'tpnc' => '{string}',
//    'catid' => '{string}',


);

$url->setQueryVariables($parameters);

$request->setMethod(HTTP_Request2::METHOD_GET);

// Request body
$request->setBody("{body}");

try
{
    $response = $request->send();
    $result = $response->getBody();

    //json_decode takes a JSON encoded string and converts it into a PHP variable
    //when true, objects are converted into associative array
    $response_decoded = json_decode($result,true);


    // check for ingredient array.
    // null coalesce operator ?? on final element means if
    // the value exists and is not null, condition proceeds. If any part of the array
    // doesn't exist, it will fail, but won't complain with warnings about undefined indexes.
    if ($response_decoded['products'][0]['ingredients'] ?? null !== null){
//    if ($response_decoded['products'][0]['ingredients'] != null){

        // can now target ingredient array
        $ingredients = $response_decoded['products'][0]['ingredients'];

        //store product name to return to user in if statement
        $productName = $response_decoded['products'][0]['description'];

        //turn contents into string so can use preg_split
        $test = implode(",", $ingredients);

        //splits ingredients with use of regex
        $ingredientList = preg_split('@[:;,.<> /)(]+@', $test);
//        print_r($ingredientList);

        //place into $data_list any matches between my ingredients db table and $ingredientList
        //$data_list becomes object with arrays
        $data_list = DB::table('ingredients') ->whereIn('name', $ingredientList)->get();

        $data_list_decoded = json_decode($data_list,true);
        $results = $data_list_decoded;

        //array_column return the values from a single column in the input array
        $ingredientName = array_column($results, 'name');

        if ($ingredientName != null) {?>

            <p><?php echo $productName ?> contains the following unsuitable ingredients:</p>

            <?php foreach($ingredientName as $ingredient) {?>

                <p><?php echo $ingredient; ?></p>

                <?php
            };

        }

        else if ($ingredientName == null) {

            echo  $productName. " does not contain any ingredients we know to be unsuitable for vegetarians. ";
        }

        else {

            echo "Error";
        }


    }

    else {

        echo  "Sorry, I am unable to locate an ingredient list. Please try again, or try an alternative product. ";

    }


}



catch (HttpException $ex)
{
    echo $ex;
}

?>



    <br><br>
    <a href="{{ url('list') }}">New Search</a>

    </section>
    </body>
</html>

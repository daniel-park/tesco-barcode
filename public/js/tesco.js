$(function() {
    var params = {
        // Request parameters
        // "gtin": "{string}",
       // "tpnb": "{string}",
       // "tpnc": "{string}",
       // "catid": "{string}",

        //Pepperoni Pizza
        "gtin": "05054402006097",

        //no ingredients array for whole foods e.g. beef:
        // "gtin": "05051277007211",

        // cod - has ingredients array as inc's water
        // "gtin": "05052004435789",

        //options are to: if possible look at string inside data.products[0].description
        //or
        // display a "what do you think?" comical response to searches with no ingredient arrays.

    };

    $.ajax({
        url: "https://dev.tescolabs.com/product/?" + $.param(params),
        beforeSend: function(xhrObj){
            // Request headers
            xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","b6dbdc2fbdef464a926aa7e76e589448");
        },
        type: "GET",
        // Request body
        data: "{body}",

    })
        .done(function(data) {
            // alert("success");
            // alert(data);
            // console.log(data);

            //get all of the object array
            // document.getElementById("demo").innerHTML = data;
            // document.getElementById("demo").innerHTML = (JSON.stringify(data));

            //get one array item of the products array
            // document.getElementById("demo").innerHTML = data.products[0].gtin
            // document.getElementById("description").innerHTML = data.products[0].description;
            // document.getElementById("ingredients").innerHTML = data.products[0].ingredients;

            //gets all ingredients within particular array item of the ingredient array
            // document.getElementById("ingredients").innerHTML = data.products[0].ingredients[2];

            // Splits ingredients of an array item which are separated by comma. Issue with results like: Mozzarella Cheese (Milk) (16%)
            // Can split at comma but then lists Mozzarella & Cheese separately
            // Unknown number of ingredients per search, need to foreach?
            // var list = data.products[0].ingredients[2];
            // var ingredient = list.split(',');
            // var ingredient1 = ingredient[0];
            // var ingredient2 = ingredient[1];
            // document.getElementById("ingredients").innerHTML = ingredient1 + ingredient2;

            // //deals with one specific defined ingredient array of the products array
            // //lists all ingredients per ingredient array
            // var list = data.products[0].ingredients[2];
            // //splits string separated by :;,.
            // var ingredient = list.split(/[:;,.]+/);
            // //lists each separated ingredient on own line
            // for (list = 0; list < ingredient.length; ++list) {
            //     console.log(ingredient[list]);
            // }






/////////// WORKING

            // document.getElementById("description").innerHTML = data.products[0].description;
            //
            // //finds all ingredient arrays. Using concat() I merge all arrays into one, to display one array of ingredients
            // var ingredientArrays = data.products[0].ingredients;
            // var l = ingredientArrays.length;
            // var list = Array();
            // for (i = 0; i < l; i++){
            //     var allIngredients = ingredientArrays[i];
            //     var ingredient = allIngredients.split(/[:;,.<> /)(]+/);
            //     list = list.concat(ingredient);
            // }
            // // console.log(list);
            // // document.getElementById("ingredients").innerHTML = list;
            // //
            //
            // var listLength = list.length
            // var text = "<ul>";
            // for (i = 0; i < listLength; i++) {
            //     text += "<li>" + list[i] + "</li>";
            // }
            // text += "</ul>";
            // document.getElementById("ingredients").innerHTML = text;
            // console.log(list);

//////////////







        })
        .fail(function() {
            alert("error");
        });
});

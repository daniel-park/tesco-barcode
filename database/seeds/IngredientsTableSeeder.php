<?php

use Illuminate\Database\Seeder;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->insert(
            [
                [
                    'name' => 'Pork',
                ],

                [
                    'name' => 'Pepperoni',
                ]
            ]
        );
    }
}
